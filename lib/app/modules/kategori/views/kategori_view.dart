import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:logbook_mobile_app/app/modules/home/controllers/home_controller.dart';
import 'package:logbook_mobile_app/app/routes/app_pages.dart';
import 'package:logbook_mobile_app/app/utils/drawer_screen.dart';

import '../controllers/kategori_controller.dart';

class KategoriView extends GetView<KategoriController> {
  var homeC = Get.find<HomeController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfff8f8f8),
      appBar: AppBar(
        title: Text(
          'Kategori',
          style: TextStyle(fontSize: 16),
        ),
        centerTitle: true,
      ),
      drawer: MyDrawer(),
      body: Builder(builder: (context) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Obx(() {
            return GridView.count(
              shrinkWrap: true,
              crossAxisCount: 2,
              // crossAxisSpacing: 2.0,
              mainAxisSpacing: 12.0,
              children: [
                GestureDetector(
                  onTap: () {
                    Get.toNamed(Routes.DETAIL_KATEGORI, arguments: "Discuss");
                  },
                  child: Container(
                    height: Get.height,
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.white38,
                          blurRadius: 10,
                        )
                      ],
                    ),
                    margin: EdgeInsets.symmetric(horizontal: 8, vertical: 10),
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 22),
                    child: Wrap(children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset(
                            "assets/logo/logo-1.png",
                            fit: BoxFit.cover,
                          ),
                          const SizedBox(height: 24),
                          Text(
                            "Discuss",
                            style: TextStyle(fontSize: 18),
                          ),
                          Text(
                            "Aktivitas ${homeC.getDataByKategori("Discuss", homeC.listData).length}",
                            style: TextStyle(
                              color: Colors.grey,
                            ),
                          )
                        ],
                      ),
                    ]),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Get.toNamed(Routes.DETAIL_KATEGORI, arguments: "Concept");
                  },
                  child: Container(
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.white38,
                          blurRadius: 10,
                        )
                      ],
                    ),
                    margin: EdgeInsets.symmetric(horizontal: 8, vertical: 10),
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 22),
                    child: Wrap(children: [
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset("assets/logo/logo-2.png",
                              fit: BoxFit.cover),
                          const SizedBox(height: 24),
                          Text(
                            "Concept",
                            style: TextStyle(fontSize: 18),
                          ),
                          Text(
                            "Aktivitas ${homeC.getDataByKategori("Concept", homeC.listData).length}",
                            style: TextStyle(
                              color: Colors.grey,
                            ),
                          )
                        ],
                      ),
                    ]),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Get.toNamed(Routes.DETAIL_KATEGORI, arguments: "Learn");
                  },
                  child: Container(
                      decoration: const BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.white38,
                            blurRadius: 10,
                          )
                        ],
                      ),
                      margin: EdgeInsets.symmetric(horizontal: 8, vertical: 10),
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 22),
                      child: Wrap(
                        children: [
                          Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Image.asset("assets/logo/logo-3.png",
                                  fit: BoxFit.cover),
                              const SizedBox(height: 24),
                              Text(
                                "Learn",
                                style: TextStyle(fontSize: 18),
                              ),
                              Text(
                                "Aktivitas ${homeC.getDataByKategori("Learn", homeC.listData).length}",
                                style: TextStyle(
                                  color: Colors.grey,
                                ),
                              )
                            ],
                          ),
                        ],
                      )),
                ),
                GestureDetector(
                  onTap: () {
                    Get.toNamed(Routes.DETAIL_KATEGORI, arguments: "Report");
                  },
                  child: Container(
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.white38,
                          blurRadius: 10,
                        )
                      ],
                    ),
                    margin: EdgeInsets.symmetric(horizontal: 8, vertical: 10),
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 22),
                    child: Wrap(children: [
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset("assets/logo/logo-4.png",
                              fit: BoxFit.cover),
                          const SizedBox(height: 24),
                          Text(
                            "Report",
                            style: TextStyle(fontSize: 18),
                          ),
                          Text(
                            "Aktivitas ${homeC.getDataByKategori("Report", homeC.listData).length}",
                            style: TextStyle(
                              color: Colors.grey,
                            ),
                          )
                        ],
                      ),
                    ]),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Get.toNamed(Routes.DETAIL_KATEGORI, arguments: "Design");
                  },
                  child: Container(
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.white38,
                          blurRadius: 10,
                        )
                      ],
                    ),
                    margin: EdgeInsets.symmetric(horizontal: 8, vertical: 10),
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 22),
                    child: Wrap(
                      children: [
                        Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Image.asset("assets/logo/logo-5.png",
                                fit: BoxFit.cover),
                            const SizedBox(height: 24),
                            Text(
                              "Design",
                              style: TextStyle(fontSize: 18),
                            ),
                            Text(
                              "Aktivitas ${homeC.getDataByKategori("Design", homeC.listData).length}",
                              style: TextStyle(
                                color: Colors.grey,
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Get.toNamed(Routes.DETAIL_KATEGORI, arguments: "Other");
                  },
                  child: Container(
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.white38,
                          blurRadius: 10,
                        )
                      ],
                    ),
                    margin: EdgeInsets.symmetric(horizontal: 8, vertical: 10),
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 22),
                    child: Wrap(
                      children: [
                        Wrap(
                          children: [
                            Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Image.asset("assets/logo/logo-6.png",
                                    fit: BoxFit.cover),
                                const SizedBox(height: 24),
                                Text(
                                  "Other",
                                  style: TextStyle(fontSize: 18),
                                ),
                                Text(
                                  "Aktivitas ${homeC.getDataByKategori("Other", homeC.listData).length}",
                                  style: TextStyle(
                                    color: Colors.grey,
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            );
          }),
        );
      }),
    );
  }
}
