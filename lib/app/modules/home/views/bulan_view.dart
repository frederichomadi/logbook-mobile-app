import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:logbook_mobile_app/app/modules/home/controllers/home_controller.dart';
import 'package:logbook_mobile_app/app/modules/home/views/card_task_view.dart';
import 'package:logbook_mobile_app/app/routes/app_pages.dart';
import 'package:logbook_mobile_app/app/utils/month_list.dart';
import 'package:shimmer/shimmer.dart';
import 'package:table_calendar/table_calendar.dart';

class BulanView extends GetView<HomeController> {
  final homeC = Get.find<HomeController>();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 20.0, left: 20.0, bottom: 24.0),
      child: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 3,
                child: Column(
                  children: [
                    Obx(() {
                      return setTableCalendar();
                    }),
                    const SizedBox(height: 23),
                    Row(
                      children: [
                        const Text(
                          "Aktivitas",
                          style: TextStyle(fontSize: 18),
                        ),
                        SizedBox(width: 13),
                        Obx(
                          () => Text(
                            "${controller.getDataByDate(controller.formatDate(controller.selectedDay.value), controller.listLogs).length}",
                            style: const TextStyle(
                                fontSize: 18, color: Colors.amber),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                    Expanded(
                      child: Obx(() {
                        var list = controller.getDataByDate(
                            controller.formatDate(controller.selectedDay.value),
                            controller.listLogs);

                        return controller.isLoading.isTrue
                            ? Shimmer.fromColors(
                                child: ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: 3,
                                  itemBuilder: ((context, index) => Container(
                                        width: Get.width,
                                        height: 100,
                                        padding: const EdgeInsets.all(12),
                                        margin: const EdgeInsets.only(top: 12),
                                        decoration: const BoxDecoration(
                                            color: Colors.white,
                                            boxShadow: [
                                              BoxShadow(
                                                  color: Color.fromARGB(
                                                      255, 218, 217, 217),
                                                  offset: Offset(4, 10),
                                                  blurRadius: 10),
                                            ]),
                                      )),
                                ),
                                baseColor: Colors.grey[300]!,
                                highlightColor: Colors.grey[100]!,
                              )
                            : list.isEmpty
                                ? Wrap(
                                    children: [
                                      Center(
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Image.asset(
                                                "assets/logo/empty.png"),
                                            const Text(
                                              "Tidak ada aktivitas hari ini",
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  color: Colors.grey),
                                            )
                                          ],
                                        ),
                                      ),
                                    ],
                                  )
                                : ListView.builder(
                                    shrinkWrap: true,
                                    itemCount: list.length,
                                    itemBuilder: (context, index) {
                                      return Dismissible(
                                        key: Key(list[index].id!),
                                        child: CardTaskView(list[index]),
                                        confirmDismiss: (direction) async {
                                          if (direction ==
                                              DismissDirection.endToStart) {
                                            Get.defaultDialog(
                                                title: "Delete Log",
                                                middleText:
                                                    "Are you sure to delete this log?",
                                                onConfirm: () {
                                                  homeC.deleteLog(
                                                      list[index].id!);
                                                },
                                                onCancel: () {
                                                  Get.back();
                                                });
                                          }
                                        },
                                        direction: DismissDirection.endToStart,
                                        background: Container(
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: const [
                                              Icon(
                                                Icons.edit_note_outlined,
                                                color: Colors.amber,
                                              ),
                                              SizedBox(width: 0),
                                              Icon(
                                                Icons.delete_outline,
                                                color: Colors.red,
                                              )
                                            ],
                                          ),
                                        ),
                                      );
                                    });
                      }),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  TableCalendar<dynamic> setTableCalendar() {
    return TableCalendar(
      locale: "id_ID",
      firstDay: DateTime(1990),
      lastDay: DateTime(2100),
      rowHeight: 40,
      focusedDay: controller.focusedDay.value,
      calendarFormat: controller.calendarFormat.value,
      calendarStyle: CalendarStyle(
          todayDecoration: BoxDecoration(color: Colors.blue.withOpacity(0.2)),
          selectedDecoration: BoxDecoration(color: Colors.blue)),
      selectedDayPredicate: (day) {
        return isSameDay(controller.selectedDay.value, day);
      },
      onDaySelected: (selectedDay, focusedDay) {
        if (!isSameDay(controller.selectedDay.value, selectedDay)) {
          controller.selectedDay.value = selectedDay;
          controller.focusedDay.value = focusedDay;
        }
      },
      onFormatChanged: (format) {
        if (controller.calendarFormat.value != format) {
          controller.calendarFormat.value = format;
        }
      },
      onPageChanged: (focusedDay) {
        controller.focusedDay.value = focusedDay;
      },
      headerStyle: HeaderStyle(
          formatButtonDecoration: BoxDecoration(
        border: Border.all(color: Colors.black, width: 1),
      )),
    );
  }
}
