import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:logbook_mobile_app/app/data/models/logs_model.dart';
import 'package:logbook_mobile_app/app/modules/home/controllers/home_controller.dart';
import 'package:logbook_mobile_app/app/routes/app_pages.dart';

class CardTaskView extends GetView<HomeController> {
  CardTaskView(this.data);
  Log data;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.toNamed(Routes.EDIT, arguments: data.id);
      },
      child: Container(
        padding: EdgeInsets.all(12),
        margin: EdgeInsets.only(top: 12),
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(
              color: Color.fromARGB(255, 218, 217, 217),
              offset: Offset(4, 10),
              blurRadius: 10),
        ]),
        child: Obx(() {
          var test = controller.isDone.value;
          return Column(
            children: [
              Row(
                children: [
                  Checkbox(
                      value: data.isDone,
                      onChanged: (value) async {
                        var connectivityResult =
                            await (Connectivity().checkConnectivity());

                        if (connectivityResult == ConnectivityResult.mobile ||
                            connectivityResult == ConnectivityResult.wifi ||
                            connectivityResult == ConnectivityResult.ethernet) {
                          data.isDone = !data.isDone!;
                          controller.updateLogState(data);
                        } else {
                          Get.defaultDialog(
                              title: "Connection Status",
                              middleText:
                                  "Your not connecting to the internet");
                        }
                      }),
                  Text(
                    "${data.target}",
                    style: TextStyle(
                      fontSize: 15,
                      decoration: data.isDone!
                          ? TextDecoration.lineThrough
                          : TextDecoration.none,
                      color: data.isDone! ? Colors.grey : Colors.black,
                    ),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.only(left: 32.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      child: Text(
                        "${data.reality}",
                        style: TextStyle(color: Colors.grey),
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.start,
                        maxLines: 2,
                        softWrap: false,
                      ),
                    ),
                    Divider(
                      color: Color(0xff509BF8),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          "${data.time}",
                          style: TextStyle(color: Color(0xff434343)),
                        ),
                        const SizedBox(width: 10),
                        Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                          decoration: BoxDecoration(
                            color: Color(0xffABE5A6).withOpacity(0.6),
                          ),
                          child: Center(
                            child: Text(
                              "${data.category}",
                              style: TextStyle(color: Color(0xff434343)),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              )
            ],
          );
        }),
      ),
    );
  }
}
