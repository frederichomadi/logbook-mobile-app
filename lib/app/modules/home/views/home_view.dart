import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:get/get.dart';
import 'package:logbook_mobile_app/app/modules/home/views/bulan_view.dart';
import 'package:logbook_mobile_app/app/routes/app_pages.dart';
import 'package:logbook_mobile_app/app/utils/drawer_screen.dart';
import 'package:logbook_mobile_app/app/utils/popmenu_screen.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfff8f8f8),
      appBar: AppBar(
        title: const Text(
          'Aktivitasku',
          style: TextStyle(fontSize: 16),
        ),
        centerTitle: true,
        actions: [PopupMenuScreen(Colors.white)],
      ),
      drawer: MyDrawer(),
      body: BulanView(),
      bottomNavigationBar: Container(
        margin: EdgeInsets.all(16),
        width: Get.width,
        height: 62,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5), color: Colors.blue),
        child: ElevatedButton(
          onPressed: () {
            Get.toNamed(Routes.ADD);
          },
          child: Text(
            "+ Tambah Aktivitas",
            style: TextStyle(
              fontSize: 18,
            ),
          ),
        ),
      ),
    );
  }
}
