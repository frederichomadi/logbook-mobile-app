import 'dart:convert';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';
import 'package:logbook_mobile_app/app/data/models/logs_model.dart';
import 'package:logbook_mobile_app/app/data/providers/logs_provider.dart';
import 'package:logbook_mobile_app/app/routes/app_pages.dart';
import 'package:logbook_mobile_app/app/utils/filter_type.dart';
import 'package:logbook_mobile_app/app/utils/month_list.dart';
import 'package:table_calendar/table_calendar.dart';

class HomeController extends GetxController {
  var listData = <Log>[].obs;
  var listLogs = <Log>[].obs;
  var focusedDay = DateTime.now().obs;
  var selectedDay = DateTime.now().obs;
  var changeFormat = false.obs;
  var calendarFormat = CalendarFormat.month.obs;
  var isTaskDelayed = false.obs;
  var isTaskDone = false.obs;
  var isAllTask = false.obs;
  var month = ''.obs;
  var isDone = false.obs;
  var isLoading = true.obs;

  final box = GetStorage();

  @override
  void onInit() async {
    _initialData();
    _loadDataFromLocal();
    listLogs.value = listData;
    setMonth(focusedDay.value.month);
    super.onInit();
  }

  _loadDataFromLocal() async {
    // listData.clear();
    var connectivityResult = await (Connectivity().checkConnectivity());
    try {
      if (connectivityResult != ConnectivityResult.mobile ||
          connectivityResult != ConnectivityResult.wifi) {
        if (box.read("logs") != null) {
          var data = box.read('logs');

          for (var value in data) {
            var log = Log(
                id: value["id"],
                target: value["target"],
                category: value["category"],
                reality: value["reality"],
                time: value["time"],
                note: value["note"],
                isDone: value["is_done"],
                date: value["date"]);
            listData.add(log);
          }
        }
      }
      isLoading.value = false;
      return listData;
    } catch (e) {
      isLoading.value = false;
      print(e);
      return listData;
    }
  }

  _initialData() {
    isLoading.value = true;
    try {
      LogsProvider().getLogs().then((response) {
        var logs = response.body as Map<String, dynamic>;

        listData.clear();
        logs.forEach(((key, value) {
          var log = Log(
              id: key,
              target: value["target"],
              category: value["category"],
              reality: value["reality"],
              time: value["time"],
              note: value["note"],
              isDone: value["is_done"],
              date: value["date"]);
          listData.add(log);
        }));
        box.write('logs', listData);
        isLoading.value = false;
        listData.refresh();

        //save data to internal storage

        print("masukan data berhasil");
      });
    } catch (e) {
      print(e);
    }
  }

  Log getDataById(String id) => listData.firstWhere((value) => value.id == id);

  dialogError(String msg) {
    Get.defaultDialog(
      title: "Terjadi Kesalahan",
      content: Text(
        msg,
        textAlign: TextAlign.center,
      ),
    );
  }

  addTask(Log log) {
    if (log != null) {
      LogsProvider().addLogs(log).then(
        (response) {
          print(response["name"]);
          final newLog = Log(
              id: response["name"],
              target: log.target,
              category: log.category,
              reality: log.reality,
              time: log.time,
              note: log.note,
              isDone: log.isDone,
              date: log.date);
          listData.add(newLog);
          listData.refresh();
          Get.back();
        },
      );
    } else {
      dialogError("Semua input harus terisi");
    }
  }

  updateLog(Log log) {
    var data = getDataById(log.id!);

    LogsProvider().updateLogs(log).then((_) {
      print(log.reality);
      data.id = log.id;
      data.category = log.category;
      data.target = log.target;
      data.reality = log.reality;
      data.time = log.time;
      data.note = log.note;
      data.isDone = log.isDone;
      data.date = log.date;
      listData.refresh();
      Get.snackbar(
        "Update Log",
        "Update log success",
        snackPosition: SnackPosition.BOTTOM,
      );
      Get.offAllNamed(Routes.HOME);
    });
  }

  updateLogState(Log log) {
    var data = getDataById(log.id!);

    LogsProvider().updateLogs(log).then((_) {
      data.date = log.date;
      listData.refresh();
      Get.snackbar(
        "Update Log",
        "Update log success",
        snackPosition: SnackPosition.BOTTOM,
      );
    });
  }

  deleteLog(String id) {
    LogsProvider()
        .deleteLogs(id)
        .then((value) => listData.removeWhere((element) => element.id == id));
    Get.back();
  }

  List<Log> getDataByDate(String date, List<Log> list) {
    List<Log> curr = [];
    try {
      curr = list.where((element) => element.date == date).toList();
    } catch (e) {
      print(e);
    }
    return curr;
  }

  List<Log> getDataByKategori(String kategori, List<Log> list) {
    List<Log> curr = [];
    curr = list.where((element) => element.category == kategori).toList();
    return curr;
  }

  String formatDate(DateTime day) {
    var format = DateFormat("EEEE, d MMMM yyyy", "id_ID");
    return format.format(day);
  }

  setMonth(int index) {
    month.value = Month().listMonth[index - 1].toString();
  }

  List<Log> filter(Filter type) {
    List<Log> list = [];
    if (type == Filter.DELAY) {
      return list = listData.where((val) => val.isDone == false).toList();
    } else if (type == Filter.DONE) {
      return list = listData.where((p0) => p0.isDone == true).toList();
    } else if (type == Filter.ALL) {
      return list = listData;
    }

    return list;
  }
}
