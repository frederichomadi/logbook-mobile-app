import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:logbook_mobile_app/app/modules/home/controllers/home_controller.dart';
import 'package:logbook_mobile_app/app/modules/home/views/card_task_view.dart';
import 'package:logbook_mobile_app/app/utils/popmenu_screen.dart';

import '../controllers/detail_kategori_controller.dart';

class DetailKategoriView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 80,
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          'Discuss',
          style: TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.w500,
            color: Colors.black,
          ),
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            size: 24,
            color: Colors.black,
          ),
          onPressed: () {
            Get.back();
          },
        ),
        actions: [PopupMenuScreen(Colors.blue)],
      ),
      body: Obx(() {
        var kategori = Get.arguments;
        return controller
                .getDataByKategori(kategori, controller.listLogs)
                .isEmpty
            ? Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      "assets/logo/empty.png",
                    ),
                    Text(
                      "Tidak ada aktivitas di kategori ini",
                      style: TextStyle(
                        color: Colors.grey,
                      ),
                    )
                  ],
                ),
              )
            : ListView.builder(
                itemCount: controller
                    .getDataByKategori(kategori, controller.listLogs)
                    .length,
                padding: EdgeInsets.all(24),
                itemBuilder: (context, index) => CardTaskView(
                  controller.getDataByKategori(
                      kategori, controller.listLogs)[index],
                ),
              );
      }),
    );
  }
}
