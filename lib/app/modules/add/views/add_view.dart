import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';
import 'package:logbook_mobile_app/app/data/models/logs_model.dart';
import 'package:logbook_mobile_app/app/modules/add/controllers/add_controller.dart';
import 'package:logbook_mobile_app/app/modules/add/kategori_model.dart';
import 'package:logbook_mobile_app/app/modules/home/controllers/home_controller.dart';

class AddView extends GetView<AddControler> {
  final homeC = Get.find<HomeController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.blue,
      ),
      child: Obx(() {
        return SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 25),
            child: ListView(
              shrinkWrap: true,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    IconButton(
                      onPressed: () {
                        Get.back();
                      },
                      icon: const Icon(Icons.arrow_back, size: 24),
                    ),
                    const Text(
                      "Detail Aktivitas",
                      style:
                          TextStyle(fontSize: 24, fontWeight: FontWeight.w500),
                    )
                  ],
                ),
                const SizedBox(height: 24),
                const Text(
                  "Target/Ekspetasi",
                  style: TextStyle(
                    color: Colors.grey,
                  ),
                ),
                TextField(
                  controller: controller.targetController,
                  decoration: InputDecoration(
                    prefixIcon: Image.asset("assets/logo/kotak.png"),
                    hintText: "Judul",
                    hintStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                    ),
                    border: OutlineInputBorder(),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                    ),
                  ),
                ),
                const SizedBox(height: 24),
                const Text(
                  "Kategori",
                  style: TextStyle(
                    color: Colors.grey,
                  ),
                ),
                TextField(
                  controller: controller.realityController,
                  minLines: 4,
                  maxLines: 10,
                  decoration: InputDecoration(
                    hintText: "Deskrpsi",
                    hintStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                    ),
                    border: OutlineInputBorder(),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                    ),
                  ),
                ),
                const SizedBox(height: 24),
                const Text(
                  "Realita",
                  style: TextStyle(
                    color: Colors.grey,
                  ),
                ),
                Obx(() {
                  return GridView.count(
                    shrinkWrap: true,
                    crossAxisCount: 3,
                    childAspectRatio: 3.0,
                    crossAxisSpacing: 16.0,
                    mainAxisSpacing: 10,
                    children: controller.listKategori
                        .map(
                          (e) => Container(
                            height: 40,
                            decoration: BoxDecoration(
                                color: controller.kategori.value == e
                                    ? Colors.blue
                                    : Colors.white,
                                border: Border.all(
                                  color: Color(0xff509BF8).withOpacity(0.3),
                                )),
                            child: ChoiceChip(
                              label: Text(e),
                              elevation: 0,
                              pressElevation: 0,
                              side: BorderSide(
                                color: controller.kategori.value == e
                                    ? Colors.blue
                                    : Colors.white,
                                width: 0,
                              ),
                              backgroundColor: Colors.white,
                              selectedColor: Colors.blue,
                              labelStyle: TextStyle(
                                  color: controller.kategori.value == e
                                      ? Colors.white
                                      : Colors.black),
                              selected: controller.kategori.value == e,
                              onSelected: (_) => controller.kategori.value = e,
                            ),
                          ),
                        )
                        .toList(),
                  );
                }),
                const SizedBox(height: 24),
                const Text(
                  "Sub-Aktivitas",
                  style: TextStyle(
                    color: Colors.grey,
                  ),
                ),
                const SizedBox(height: 8),
                Obx(
                  () => controller.listSub.isEmpty
                      ? Padding(padding: EdgeInsets.zero)
                      : ListView.builder(
                          shrinkWrap: true,
                          itemCount: controller.listSub.length,
                          itemBuilder: ((context, index) {
                            return GestureDetector(
                              onTap: () {},
                              child: Container(
                                child: Row(
                                  children: [
                                    Icon(Icons.menu),
                                    Checkbox(
                                        value: false, onChanged: (value) {}),
                                    Text("${controller.listSub[index]}")
                                  ],
                                ),
                              ),
                            );
                          }),
                        ),
                ),
                TextButton(
                  style: TextButton.styleFrom(alignment: Alignment.topLeft),
                  onPressed: () {
                    Get.bottomSheet(
                        Padding(
                          padding: EdgeInsets.all(16),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              TextField(
                                controller: controller.subAktivitas,
                                decoration: InputDecoration(
                                    label: Text("Nama Sub-AKtivitas"),
                                    border: OutlineInputBorder()),
                              ),
                              const SizedBox(height: 16),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  ElevatedButton(
                                    onPressed: () {
                                      controller.listSub.add(
                                          controller.subAktivitas.value.text);
                                      Get.back();
                                      controller.subAktivitas.clear();
                                    },
                                    child: Text("Simpan"),
                                  ),
                                  const SizedBox(width: 16),
                                  ElevatedButton(
                                    onPressed: () {
                                      Get.back();
                                      controller.subAktivitas.clear();
                                    },
                                    child: Text("Batal"),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                        backgroundColor: Colors.white);
                  },
                  child: const Text(
                    "+ Tambah Sub-Aktivitas",
                    style: TextStyle(
                      color: Colors.black45,
                    ),
                    textAlign: TextAlign.start,
                  ),
                ),
                const SizedBox(height: 24),
                const Text(
                  "Waktu & tanggal",
                  style: TextStyle(
                    color: Colors.grey,
                  ),
                ),
                Obx(
                  () => DropdownSearch<String>(
                    mode: Mode.MENU,
                    items: controller.listWaktu,
                    hint: "Pilih Waktu",
                    popupItemDisabled: (String s) => s.startsWith('I'),
                    selectedItem: controller.waktu.value,
                    onChanged: (value) => controller.waktu.value = value!,
                    dropDownButton: Icon(Icons.keyboard_arrow_down_outlined),
                  ),
                ),
                const SizedBox(height: 8),
                Obx(
                  () => GestureDetector(
                    onTap: () {
                      controller.selectDate(context);
                    },
                    child: Container(
                      height: 60,
                      width: Get.width,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(2),
                        border: Border.all(color: Colors.grey),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "${controller.formatDate()}",
                              style: TextStyle(fontSize: 16),
                            ),
                            Icon(Icons.calendar_today_outlined)
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  width: Get.width,
                  height: 62,
                  margin: EdgeInsets.symmetric(vertical: 32),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.blue),
                  child: ElevatedButton(
                    onPressed: () async {
                      var connectivityResult =
                          await (Connectivity().checkConnectivity());

                      if (connectivityResult == ConnectivityResult.mobile ||
                          connectivityResult == ConnectivityResult.wifi ||
                          connectivityResult == ConnectivityResult.ethernet) {
                        var log = Log(
                          category: controller.kategori.value,
                          isDone: false,
                          note: "",
                          reality: controller.realityController.text,
                          target: controller.targetController.text,
                          time: controller.waktu.value,
                          date: controller.formatDate(),
                        );

                        homeC.addTask(log);
                      } else {
                        Get.defaultDialog(
                            title: "Connection Status",
                            middleText: "Your not connecting to the internet");
                      }
                    },
                    child: Text(
                      "Simpan Aktivitas",
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      }),
    ));
  }
}
