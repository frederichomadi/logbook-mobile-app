import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:logbook_mobile_app/app/modules/add/kategori_model.dart';

class AddControler extends GetxController {
  var listKategori = List<String>.empty().obs;
  var isSelected = false.obs;
  var selectedDate = DateTime.now().obs;
  var listSub = List<String>.empty().obs;
  var kategori = "".obs;

  late TextEditingController subAktivitas;

  late TextEditingController targetController;
  late TextEditingController realityController;

  final waktu = 'Pilih Waktu'.obs;
  var listWaktu = [
    "Sebelum Dzuhur",
    "Setelah Dzuhur",
    "Setelah Ashar",
    "Overtime",
  ];

  @override
  void onInit() {
    subAktivitas = TextEditingController();
    targetController = TextEditingController();
    realityController = TextEditingController();

    _initData();
    super.onInit();
  }

  _initData() {
    listKategori
        .addAll(["Concept", "Design", "Discuss", "Learn", "Report", "Other"]);
    // var concept = Kategori("Concept", isSelected.value);
    // var design = Kategori("Design", isSelected.value);
    // var discuss = Kategori("Discuss", isSelected.value);
    // var learn = Kategori("Learn", isSelected.value);
    // var report = Kategori("Report", isSelected.value);
    // var other = Kategori("Other", isSelected.value);
    // listKategori.add(concept);
    // listKategori.add(design);
    // listKategori.add(discuss);
    // listKategori.add(learn);
    // listKategori.add(report);
    // listKategori.add(other);
  }

  selectDate(BuildContext context) async {
    final selected = await showDatePicker(
      context: context,
      initialDate: selectedDate.value,
      firstDate: DateTime(2000),
      lastDate: DateTime(2100),
    );
    if (selected != null && selected != selectedDate.value) {
      selectedDate.value = selected;
    }
  }

  String formatDate() {
    var format = DateFormat("EEEE, d MMMM yyyy", "id_ID");
    return format.format(selectedDate.value);
  }

  @override
  void onClose() {
    subAktivitas.dispose();
    targetController.dispose();
    realityController.dispose();
    super.onClose();
  }
}
