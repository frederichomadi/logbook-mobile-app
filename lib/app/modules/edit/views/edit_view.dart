import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';
import 'package:logbook_mobile_app/app/data/models/logs_model.dart';
import 'package:logbook_mobile_app/app/modules/add/controllers/add_controller.dart';
import 'package:logbook_mobile_app/app/modules/edit/controllers/edit_controller.dart';
import 'package:logbook_mobile_app/app/modules/home/controllers/home_controller.dart';

class EditView extends GetView<EditController> {
  final homeC = Get.find<HomeController>();

  @override
  Widget build(BuildContext context) {
    var data = homeC.getDataById(Get.arguments);
    return Scaffold(
        body: AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.blue,
      ),
      child: Obx(() {
        controller.targetController.text = data.target!;
        controller.realitaController.text = data.reality!;
        controller.waktu.value = data.time!;
        controller.date.value = data.date!;
        controller.kategori.value = data.category!;
        return SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 25),
            child: ListView(
              shrinkWrap: true,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    IconButton(
                      onPressed: () {
                        Get.back();
                      },
                      icon: const Icon(Icons.arrow_back, size: 24),
                    ),
                    const Text(
                      "Detail Aktivitas",
                      style:
                          TextStyle(fontSize: 24, fontWeight: FontWeight.w500),
                    )
                  ],
                ),
                const SizedBox(height: 24),
                const Text(
                  "Target/Ekspetasi",
                  style: TextStyle(
                    color: Colors.grey,
                  ),
                ),
                TextField(
                  controller: controller.targetController,
                  decoration: InputDecoration(
                    prefixIcon: Image.asset("assets/logo/kotak.png"),
                    hintText: "Judul",
                    hintStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                    ),
                    border: OutlineInputBorder(),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                    ),
                  ),
                ),
                const SizedBox(height: 24),
                const Text(
                  "Realita",
                  style: TextStyle(
                    color: Colors.grey,
                  ),
                ),
                TextField(
                  minLines: 4,
                  maxLines: 10,
                  controller: controller.realitaController,
                  decoration: InputDecoration(
                    hintText: "Deskrpsi",
                    hintStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                    ),
                    border: OutlineInputBorder(),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                    ),
                  ),
                ),
                const SizedBox(height: 24),
                const Text(
                  "Realita",
                  style: TextStyle(
                    color: Colors.grey,
                  ),
                ),
                Obx(() {
                  return GridView.count(
                    shrinkWrap: true,
                    crossAxisCount: 3,
                    childAspectRatio: 3.0,
                    crossAxisSpacing: 16.0,
                    mainAxisSpacing: 10,
                    children: controller.listKategori
                        .map(
                          (e) => Container(
                            height: 40,
                            decoration: BoxDecoration(
                                color: controller.kategori.value == e
                                    ? Colors.blue
                                    : Colors.white,
                                border: Border.all(
                                  color: Color(0xff509BF8).withOpacity(0.3),
                                )),
                            child: ChoiceChip(
                              label: Text(e),
                              elevation: 0,
                              pressElevation: 0,
                              side: BorderSide(
                                color: controller.kategori.value == e
                                    ? Colors.blue
                                    : Colors.white,
                                width: 0,
                              ),
                              backgroundColor: Colors.white,
                              selectedColor: Colors.blue,
                              labelStyle: TextStyle(
                                  color: controller.kategori.value == e
                                      ? Colors.white
                                      : Colors.black),
                              selected: controller.kategori.value == e,
                              onSelected: (_) => controller.kategori.value = e,
                            ),
                          ),
                        )
                        .toList(),
                  );
                }),
                const SizedBox(height: 24),
                const Text(
                  "Sub-Aktivitas",
                  style: TextStyle(
                    color: Colors.grey,
                  ),
                ),
                const SizedBox(height: 8),
                Obx(
                  () => controller.listSub.isEmpty
                      ? Padding(padding: EdgeInsets.zero)
                      : ListView.builder(
                          shrinkWrap: true,
                          itemCount: controller.listSub.length,
                          itemBuilder: ((context, index) {
                            return GestureDetector(
                              onTap: () {},
                              child: Container(
                                child: Row(
                                  children: [
                                    Icon(Icons.menu),
                                    Checkbox(
                                        value: false, onChanged: (value) {}),
                                    Text("${controller.listSub[index]}")
                                  ],
                                ),
                              ),
                            );
                          }),
                        ),
                ),
                TextButton(
                  style: TextButton.styleFrom(alignment: Alignment.topLeft),
                  onPressed: () {
                    Get.bottomSheet(
                        Padding(
                          padding: EdgeInsets.all(16),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              TextField(
                                controller: controller.subAktivitas,
                                decoration: InputDecoration(
                                    label: Text("Nama Sub-AKtivitas"),
                                    border: OutlineInputBorder()),
                              ),
                              const SizedBox(height: 16),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  ElevatedButton(
                                    onPressed: () {
                                      controller.listSub.add(
                                          controller.subAktivitas.value.text);
                                      Get.back();
                                    },
                                    child: Text("Simpan"),
                                  ),
                                  const SizedBox(width: 16),
                                  ElevatedButton(
                                    onPressed: () {
                                      Get.back();
                                    },
                                    child: Text("Batal"),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                        backgroundColor: Colors.white);
                  },
                  child: const Text(
                    "+ Tambah Sub-Aktivitas",
                    style: TextStyle(
                      color: Colors.black45,
                    ),
                    textAlign: TextAlign.start,
                  ),
                ),
                const SizedBox(height: 24),
                const Text(
                  "Waktu & tanggal",
                  style: TextStyle(
                    color: Colors.grey,
                  ),
                ),
                Obx(
                  () => DropdownSearch<String>(
                    mode: Mode.MENU,
                    items: controller.listWaktu,
                    hint: "Pilih Waktu",
                    popupItemDisabled: (String s) => s.startsWith('I'),
                    onChanged: (value) => controller.waktu.value = value!,
                    selectedItem: controller.waktu.value,
                    dropDownButton: Icon(Icons.keyboard_arrow_down_outlined),
                  ),
                ),
                const SizedBox(height: 8),
                GestureDetector(
                  onTap: () {
                    controller.selectDate(context, homeC.focusedDay.value);
                  },
                  child: Container(
                    height: 60,
                    width: Get.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(2),
                      border: Border.all(color: Colors.grey),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "${homeC.formatDate(homeC.focusedDay.value)}",
                            style: TextStyle(fontSize: 16),
                          ),
                          Icon(Icons.calendar_today_outlined)
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  width: Get.width,
                  height: 62,
                  margin: EdgeInsets.symmetric(vertical: 32),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.blue),
                  child: ElevatedButton(
                    onPressed: () async {
                      var connectivityResult =
                          await (Connectivity().checkConnectivity());

                      if (connectivityResult == ConnectivityResult.mobile ||
                          connectivityResult == ConnectivityResult.wifi ||
                          connectivityResult == ConnectivityResult.ethernet) {
                        var log = Log(
                          id: data.id,
                          category: controller.kategori.value,
                          isDone: data.isDone,
                          note: data.note,
                          reality: controller.realitaController.text,
                          target: controller.targetController.text,
                          time: controller.waktu.value,
                          date: controller.formatDate(homeC.focusedDay.value),
                        );

                        homeC.updateLog(log);
                      } else {
                        Get.defaultDialog(
                            title: "Connection Status",
                            middleText: "Your not connecting to the internet");
                      }
                    },
                    child: Text(
                      "Simpan Aktivitas",
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      }),
    ));
  }
}
