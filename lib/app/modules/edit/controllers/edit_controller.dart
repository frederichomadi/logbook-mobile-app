import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:logbook_mobile_app/app/modules/add/kategori_model.dart';

class EditController extends GetxController {
  var listKategori = List<String>.empty().obs;
  var isSelected = false.obs;
  var selectedDate = DateTime.now().obs;
  var listSub = List<String>.empty().obs;
  var date = "".obs;
  var kategori = "".obs;

  late TextEditingController subAktivitas;

  late TextEditingController targetController;
  late TextEditingController realitaController;
  late TextEditingController waktuController;

  final waktu = 'Pilih Waktu'.obs;
  var listWaktu = [
    "Sebelum Dzuhur",
    "Setelah Dzuhur",
    "Setelah Ashar",
    "Overtime",
  ];

  @override
  void onInit() {
    subAktivitas = TextEditingController();
    targetController = TextEditingController();
    realitaController = TextEditingController();
    waktuController = TextEditingController();

    _initData();
    super.onInit();
  }

  _initData() {
    listKategori
        .addAll(["Concept", "Design", "Discuss", "Learn", "Report", "Other"]);
  }

  selectDate(BuildContext context, DateTime day) async {
    final selected = await showDatePicker(
      context: context,
      initialDate: day,
      firstDate: DateTime(2000),
      lastDate: DateTime(2100),
    );
    if (selected != null && selected != selectedDate.value) {
      selectedDate.value = selected;
    }
  }

  String formatDate(DateTime day) {
    var format = DateFormat("EEEE, d MMMM yyyy", "id_ID");
    return format.format(day);
  }

  @override
  void onClose() {
    subAktivitas.dispose();
    targetController.dispose();
    realitaController.dispose();
    waktuController.dispose();
    super.onClose();
  }
}
