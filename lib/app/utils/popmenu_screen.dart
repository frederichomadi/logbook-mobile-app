import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:logbook_mobile_app/app/data/models/logs_model.dart';
import 'package:logbook_mobile_app/app/modules/home/controllers/home_controller.dart';
import 'package:logbook_mobile_app/app/utils/filter_type.dart';

class PopupMenuScreen extends GetView<HomeController> {
  PopupMenuScreen(this.color);
  Color? color;

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      icon: Image.asset(
        "assets/logo/menu.png",
        color: color,
      ),
      enableFeedback: true,
      itemBuilder: ((context) => [
            PopupMenuItem(
              height: 0,
              padding: EdgeInsets.zero,
              child: Obx(() {
                return GestureDetector(
                  onTap: () async {
                    controller.isTaskDelayed.toggle();
                    controller.isTaskDone.value = false;
                    controller.isAllTask.value = false;
                    if (controller.isTaskDelayed.isTrue) {
                      controller.listLogs.value =
                          controller.filter(Filter.DELAY);
                    } else {
                      controller.listLogs.value = controller.filter(Filter.ALL);
                    }
                    Get.back();
                  },
                  child: Container(
                    height: 34,
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          "Aktivitas Tertunda",
                          style: TextStyle(fontSize: 12, color: Colors.black),
                        ),
                        controller.isTaskDelayed.isTrue
                            ? const Icon(
                                Icons.done,
                                color: Colors.green,
                              )
                            : const Text("")
                      ],
                    ),
                  ),
                );
              }),
            ),
            PopupMenuItem(
              height: 0,
              padding: EdgeInsets.zero,
              child: Obx(() {
                return GestureDetector(
                  onTap: () {
                    controller.isTaskDelayed.value = false;
                    controller.isTaskDone.toggle();
                    controller.isAllTask.value = false;
                    if (controller.isTaskDone.isTrue) {
                      controller.listLogs.value =
                          controller.filter(Filter.DONE);
                    } else {
                      controller.listLogs.value = controller.filter(Filter.ALL);
                    }
                    Get.back();
                  },
                  child: Container(
                    height: 34,
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          "Aktivitas Selesai",
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.black,
                          ),
                        ),
                        controller.isTaskDone.isTrue
                            ? const Icon(
                                Icons.done,
                                color: Colors.green,
                              )
                            : const Text("")
                      ],
                    ),
                  ),
                );
              }),
            ),
            PopupMenuItem(
              height: 0,
              padding: EdgeInsets.zero,
              child: Obx(() {
                return GestureDetector(
                  onTap: () {
                    controller.isTaskDelayed.value = false;
                    controller.isTaskDone.value = false;
                    controller.isAllTask.toggle();
                    controller.listLogs.value = controller.filter(Filter.ALL);
                    Get.back();
                  },
                  child: Container(
                    height: 35,
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          "Semua Aktivitas",
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.black,
                          ),
                        ),
                        controller.isAllTask.isTrue
                            ? const Icon(
                                Icons.done,
                                color: Colors.green,
                              )
                            : const Text("")
                      ],
                    ),
                  ),
                );
              }),
            )
          ]),
    );
  }
}
