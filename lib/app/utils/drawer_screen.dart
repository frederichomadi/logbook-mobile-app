import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:logbook_mobile_app/app/modules/home/controllers/home_controller.dart';
import 'package:logbook_mobile_app/app/routes/app_pages.dart';
import 'package:logbook_mobile_app/app/utils/filter_type.dart';

class MyDrawer extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Drawer(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                Container(
                  height: 64,
                  width: Get.width,
                  decoration: const BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        color: Color(0xff509BF8),
                        width: 1,
                      ),
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: Row(
                      children: [
                        Image.asset(
                          "assets/logo/mini_logo.png",
                          width: 21,
                          height: 21,
                        ),
                        const SizedBox(width: 8),
                        const Text(
                          "Logbook App",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(height: 24),
                InkWell(
                  onTap: () {
                    controller.isAllTask.value = false;
                    controller.isTaskDelayed.value = false;
                    controller.isTaskDone.value = false;
                    controller.listLogs.value = controller.filter(Filter.ALL);
                    Get.toNamed(Routes.HOME);
                  },
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: Row(
                      children: [
                        Image.asset("assets/logo/activity.png"),
                        const SizedBox(width: 8),
                        const Text("Laporan Aktivitas")
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    controller.isAllTask.value = false;
                    controller.isTaskDelayed.value = false;
                    controller.isTaskDone.value = false;
                    controller.listLogs.value = controller.filter(Filter.ALL);
                    Get.toNamed(Routes.KATEGORI);
                  },
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: Row(
                      children: [
                        Image.asset("assets/logo/category.png"),
                        const SizedBox(width: 8),
                        const Text("Kategori")
                      ],
                    ),
                  ),
                ),
              ],
            ),
            InkWell(
              onTap: () {},
              child: Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 24,
                  vertical: 12,
                ),
                margin: EdgeInsets.only(
                    bottom: context.mediaQueryPadding.bottom + 20),
                child: Row(
                  children: const [
                    Icon(
                      Icons.logout_outlined,
                      size: 15,
                    ),
                    SizedBox(width: 8),
                    Text("Keluar"),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
