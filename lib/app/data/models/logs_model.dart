class Log {
  String? id;
  String? category;
  bool? isDone;
  String? note;
  String? reality;
  String? target;
  String? time;
  String? date;

  Log(
      {this.id,
      this.category,
      this.isDone,
      this.note,
      this.reality,
      this.target,
      this.time,
      this.date});

  Log.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    category = json['category'];
    isDone = json['is_done'];
    note = json['note'];
    reality = json['reality'];
    target = json['target'];
    time = json['time'];
    date = json['date'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['category'] = category;
    data['is_done'] = isDone;
    data['note'] = note;
    data['reality'] = reality;
    data['target'] = target;
    data['time'] = time;
    data['date'] = date;
    return data;
  }
}
