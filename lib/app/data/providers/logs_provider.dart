import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:logbook_mobile_app/app/utils/constants.dart';

import '../models/logs_model.dart';

class LogsProvider extends GetConnect {
  Future<dynamic> getLogs() async => await get("${BASE_URL}logs.json");

  Future<dynamic> addLogs(Log log) async {
    var response = await post("${BASE_URL}logs.json", {
      "target": log.target,
      "category": log.category,
      "reality": log.reality,
      "time": log.time,
      "note": log.note,
      "is_done": log.isDone,
      "date": log.date
    });
    return response.body;
  }

  Future<void> updateLogs(Log log) async {
    await put("${BASE_URL}logs/${log.id}.json", {
      "target": log.target,
      "category": log.category,
      "reality": log.reality,
      "time": log.time,
      "note": log.note,
      "is_done": log.isDone,
      "date": log.date
    });
  }

  Future<void> deleteLogs(String id) async =>
      await delete("${BASE_URL}logs/${id}.json");
}
