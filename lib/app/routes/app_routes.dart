part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const KATEGORI = _Paths.KATEGORI;
  static const DETAIL_KATEGORI = _Paths.DETAIL_KATEGORI;
  static const EDIT = _Paths.EDIT;
  static const ADD = _Paths.ADD;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const KATEGORI = '/kategori';
  static const DETAIL_KATEGORI = '/detail-kategori';
  static const EDIT = '/edit';
  static const ADD = '/add';
}
