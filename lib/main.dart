import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:logbook_mobile_app/app/utils/splash_screen.dart';

import 'app/routes/app_pages.dart';

void main() async {
  await GetStorage.init();
  initializeDateFormatting().then(
    (_) => runApp(
      FutureBuilder(
        future: Future.delayed(
          Duration(seconds: 3),
        ),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return GetMaterialApp(
              debugShowCheckedModeBanner: false,
              title: "Application",
              theme: ThemeData(
                fontFamily: "Kanit",
              ),
              themeMode: ThemeMode.light,
              initialRoute: Routes.HOME,
              getPages: AppPages.routes,
            );
          }

          return SplashScreen();
        },
      ),
    ),
  );
}
